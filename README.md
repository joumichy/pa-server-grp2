# Improved Youtube search (Server part)

This poject is the main server of our application which give endpoint to allow different interactions between the user interface (Flutter Application), and the back (Java Client)

### Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. 

### Prerequisites

+ Install MySQL, go to https://www.mysql.com/fr/ to know more about it.
+ Go to https://azure.microsoft.com/fr-fr/ to create an azure account(you can create a free account for a try out)
+ Create an Azure Storage which have a feature called Queue and get the access key
+ In your application.properties file add this environment variables :
    + DATABASE_HOSTNAME = host of your database
    + DATABASE_PASSWORD = password of your database
    + DATABASE_USERNAME = username of your database
    + JWT_SECRET_TOKEN = a String which represent the secrect token
    + JWT_EXPIRATION_TOKEN = a String which represent the duration of the token generated
    + STORAGE_CONNECTION_STRING = Access key of your Queue in Azure Storage
    + SEND_TASKS_QUEUE = a String which contain the beginning name of a Queue

    
### Installing 
Install Maven on your computer, go to https://maven.apache.org/ to know more about it and how to install it.

In the directory of the project :
* if you're using IntelliJ, all you need to do is to build and run your project
* if you're on a terminal use the command line "maven package", "maven test" and "maven mvn exec:java -Dexec.mainClass=com.server.grp2.ApplicationServer"
 
### Running the tests

Use the command line "maven test"

# Build with
+ Maven
+ Spring 
+ Java Enterprise Edition

# Author

Student of ESGI - GHALEM Marc - 4AL1 - Group 2