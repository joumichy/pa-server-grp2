package com.server.grp2.User.infrastructure.dao;

import com.server.grp2.User.domain.model.RoleInfo;
import com.server.grp2.User.domain.model.User;
import com.server.grp2.User.domain.model.UserException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class UserAdapter {

    private static PasswordEncoder encoder;

    public UserAdapter(PasswordEncoder encoder){
        UserAdapter.encoder = encoder;
    }

    public static UserEntity convertToUserEntity(User user, RoleEntity role) {
        return new UserEntity(
                user.getUsername(),
                encoder.encode(user.getPassword()),
                user.getFirstName(),
                user.getLastName(),
                user.getMail(),
                true,
                role);
    }

    public static User convertToUser(UserEntity userEntity) throws UserException {
        RoleInfo role = new RoleInfo(userEntity.getRoleEntity().getRoleName());
        return new User(
                userEntity.getUserId().toString(),
                userEntity.getUsername(),
                userEntity.getPassword(),
                userEntity.getFirstName(),
                userEntity.getLastName(),
                userEntity.getMail(),
                userEntity.getEnabled(),
                role);
    }
}
