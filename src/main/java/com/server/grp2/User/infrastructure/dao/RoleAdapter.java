package com.server.grp2.User.infrastructure.dao;

import com.server.grp2.User.domain.model.EnumRoles;
import com.server.grp2.User.domain.model.RoleException;

public class RoleAdapter {

    public static RoleEntity convertToRoleEntity(String roleInfo) throws RoleException {

        if (roleInfo.equalsIgnoreCase("user") || roleInfo.equalsIgnoreCase(EnumRoles.ROLE_USER.toString()))
            return new RoleEntity(EnumRoles.ROLE_USER.toString());

        if (roleInfo.equalsIgnoreCase("admin") || roleInfo.equalsIgnoreCase(EnumRoles.ROLE_ADMIN.toString()))
            return new RoleEntity(EnumRoles.ROLE_ADMIN.toString());

        throw new RoleException("RoleEntity not found");
    }

}
