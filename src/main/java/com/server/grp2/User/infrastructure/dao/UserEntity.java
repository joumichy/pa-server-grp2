package com.server.grp2.User.infrastructure.dao;

import javax.persistence.*;
import javax.validation.constraints.*;

@Entity
@Table(name = "users",
        uniqueConstraints = {
                @UniqueConstraint(columnNames = "username"),
                @UniqueConstraint(columnNames = "mail")
        })
public class UserEntity {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "user_id")
    private long id;

    @NotEmpty(message = "Username field is empty !")
    private String username;

    @NotBlank
    private String password;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_Name")
    private String lastName;

    @Column(name = "Mail")
    private String mail;

    private Boolean enabled;

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinTable(
            name="user_role",
            joinColumns=@JoinColumn(name="user_id"),
            inverseJoinColumns={@JoinColumn(name="role_id")})
    private RoleEntity roleEntity;

    @Override
    public String toString() {
        return "UserEntity{" +
                "userId=" + id +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", mail='" + mail + '\'' +
                ", enabled=" + enabled +
                ", roles=" + roleEntity +
                '}';
    }
    public UserEntity(String username, String password, String firstName, String lastName, String mail, boolean enabled, RoleEntity roleEntity){
        this.username = username;
        this.password = password;
        this.firstName = firstName;
        this.lastName =lastName;
        this.mail = mail;
        this.enabled = enabled;
        this.roleEntity = roleEntity;
    }

    public UserEntity(){}

    public Long getUserId() {
        return id;
    }

    public String getUsername() {
            return username;
        }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getMail() {
        return mail;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public RoleEntity getRoleEntity() {
        return roleEntity;
    }

    public void updateId(long parseLong) {
        this.id = parseLong;
    }
}
