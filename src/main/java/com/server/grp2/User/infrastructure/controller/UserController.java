package com.server.grp2.User.infrastructure.controller;

import com.server.grp2.User.domain.model.RoleException;
import com.server.grp2.User.domain.model.User;
import com.server.grp2.User.domain.model.UserException;
import com.server.grp2.User.use_cases.CreateUser;
import com.server.grp2.User.use_cases.DeleteUser;
import com.server.grp2.User.use_cases.GetUser;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;


@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/user")
public class UserController {

    private final CreateUser createUser;

    private final DeleteUser deleteUser;

    private final GetUser getUser;

    public UserController(CreateUser createUser, DeleteUser deleteUser, GetUser getUser) {
        this.createUser = createUser;
        this.deleteUser = deleteUser;
        this.getUser = getUser;
    }

    @PostMapping("/signup")
    public ResponseEntity<?> registerUser(@Valid @RequestBody UserDto userDto) {

        try {
            createUser.execute(
                userDto.getUsername(),
                userDto.getMail(),
                userDto.getFirstName(),
                userDto.getLastName(),
                userDto.getPassword(),
                userDto.getRole());

        } catch (UserException | RuntimeException | RoleException e) {
            return new ResponseEntity<>(
                    e.getMessage(),
                    HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(
                "User registered successfully !",
                HttpStatus.CREATED);
    }

    @DeleteMapping("/delete/")
    public ResponseEntity<?> deleteUser(Principal currentUser) {
        if(currentUser==null)
            return new ResponseEntity<>(
                    "User doesn't exist ",
                    HttpStatus.BAD_REQUEST);
        try {
            deleteUser.execute(currentUser.getName());
            return new ResponseEntity<>(
                    "User deleted successfully ! ",
                    HttpStatus.OK);
        } catch (UserException e) {
            return new ResponseEntity<>(
                    e.getMessage(),
                    HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping
    public ResponseEntity<?> getUserInfo(Principal curentUser) {
        if(curentUser==null)
            return new ResponseEntity<>(
                    "Any Token given...",
                    HttpStatus.BAD_REQUEST);
        try {
            User user = getUser.execute(curentUser.getName());
            return new ResponseEntity<>(
                    user,
                    HttpStatus.OK);
        } catch (UserException e) {
            return new ResponseEntity<>(
                    e.getMessage(),
                    HttpStatus.BAD_REQUEST);
        }
    }

}
