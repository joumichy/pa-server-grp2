package com.server.grp2.User.infrastructure.dao;

import com.server.grp2.User.domain.model.RoleException;
import com.server.grp2.User.domain.model.UserException;
import com.server.grp2.User.domain.model.User;
import com.server.grp2.User.domain.UserRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserJpaRepository extends JpaRepository<UserEntity, Long>, UserRepository {

    default void addUser(User user) throws UserException, RoleException {

        if (existsByUsername(user.getUsername())) throw new UserException(user.getUsername() + " already use !");
        if (existsByMail(user.getMail())) throw new UserException(user.getMail() + " already use !");
        RoleEntity roleEntity = RoleAdapter.convertToRoleEntity(user.getRole().getRoleName());
        UserEntity userEntity = UserAdapter.convertToUserEntity(user, roleEntity);
        UserEntity userSaved = save(userEntity);
        UserAdapter.convertToUser(userSaved);
    }

    default boolean deleteUser(String username)throws UserException {

        List<UserEntity> allUser = findAll();

        for(UserEntity user : allUser){
            if (user.getUsername().equals(username)){
                delete(user);
                return true;
            }
        }
        throw new UserException("User doesn't exist ") ;

    }

    default User getUserInfo(String username) throws UserException {
        List<UserEntity> allUserEntities = findAll();
        UserEntity byUsername;

        for (UserEntity userEntity : allUserEntities) {
            if (userEntity.getUsername().equals(username)) {
                byUsername = userEntity;
                return UserAdapter.convertToUser(byUsername);
            }
        }
        throw new UserException("User with username "+ username + " not found !");
    }

    default User getUserInfo(int userId) throws UserException {
        List<UserEntity> allUserEntities = findAll();
        UserEntity byId;

        for (UserEntity userEntity : allUserEntities) {
            if (userEntity.getUserId() == userId) {
                byId = userEntity;
                return UserAdapter.convertToUser(byId);
            }
        }
        throw new UserException("User with id "+ userId + " not found !");

    }

}

