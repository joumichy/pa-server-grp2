package com.server.grp2.User.use_cases;

import com.server.grp2.User.domain.UserRepository;
import com.server.grp2.User.domain.model.User;
import com.server.grp2.User.domain.model.UserException;
import org.springframework.stereotype.Service;

@Service
public class GetUser {

    private final UserRepository userRepository;

    public GetUser(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User execute(String username) throws UserException {
        return userRepository.getUserInfo(username);
    }

    public User execute(int idUser) throws UserException {
        return userRepository.getUserInfo(idUser);
    }
}