package com.server.grp2.User.use_cases;

import com.server.grp2.User.domain.UserRepository;
import com.server.grp2.User.domain.model.UserException;
import org.springframework.stereotype.Service;

@Service
public class DeleteUser {

    private final UserRepository userRepository;

    public DeleteUser(UserRepository userRepository){
        this.userRepository = userRepository;
    }

    public boolean execute(String username) throws UserException {
        return userRepository.deleteUser(username);
    }

}
