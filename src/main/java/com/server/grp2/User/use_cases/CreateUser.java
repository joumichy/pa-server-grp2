package com.server.grp2.User.use_cases;

import com.server.grp2.User.domain.UserRepository;
import com.server.grp2.User.domain.model.RoleException;
import com.server.grp2.User.domain.model.RoleInfo;
import com.server.grp2.User.domain.model.User;
import com.server.grp2.User.domain.model.UserException;
import org.springframework.stereotype.Service;

@Service
public class CreateUser {

    private final UserRepository userRepository;

    public CreateUser(UserRepository userRepository){
        this.userRepository = userRepository;
    }

    public void execute(String username, String mail, String firstname, String lastName, String password, String role) throws UserException, RoleException {
        RoleInfo roleInfo = new RoleInfo(role);
        User user = new User(null, username, password, firstname, lastName,mail, true, roleInfo);
        userRepository.addUser(user);
    }
}
