package com.server.grp2.User.domain.model;

public class User {

    private final String id;

    private final String username;

    private final String password;

    private final String firstName;

    private final String lastName;

    private final String mail;

    private final Boolean enabled;

    private final RoleInfo role;

    public User(String id, String username, String password, String firstName, String lastName, String mail, Boolean enabled, RoleInfo role) throws UserException {
        this.id = id;
        this.username = username;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.mail = mail;
        this.enabled = enabled;
        this.role = role;

        this.checkFields();
    }

    public String getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getMail() {
        return mail;
    }

    public RoleInfo getRole() {
        return role;
    }

    private void checkFields() throws UserException {
        if (username.isEmpty() || password.isEmpty() || mail.isEmpty())
            throw new UserException("Fields missing !");
    }

}
