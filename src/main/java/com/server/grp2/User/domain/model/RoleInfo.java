package com.server.grp2.User.domain.model;

public class RoleInfo {

    private String roleName;

    public RoleInfo(String roleName) {
        this.roleName = roleName;
    }

    public String getRoleName() {
        return roleName;
    }

    public String toString() {
        return "RoleEntity {" +
                " roleName='" + roleName + '\'' +
                '}';
    }
}