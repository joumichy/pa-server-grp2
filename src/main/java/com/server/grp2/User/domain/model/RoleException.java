package com.server.grp2.User.domain.model;

public class RoleException extends Exception {

    public RoleException(String message) {
        super(message);
    }
}