package com.server.grp2.User.domain.model;

public class UserException extends Exception {

    public UserException(String message) {
        super(message);
    }
}
