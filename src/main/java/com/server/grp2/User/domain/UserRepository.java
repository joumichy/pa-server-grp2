package com.server.grp2.User.domain;

import com.server.grp2.User.domain.model.RoleException;
import com.server.grp2.User.domain.model.User;
import com.server.grp2.User.domain.model.UserException;

public interface UserRepository {
    void addUser(User user) throws UserException, RoleException;
    boolean deleteUser(String username) throws UserException ;
    Boolean existsByUsername(String username);
    Boolean existsByMail(String mail);
    User getUserInfo(String username) throws UserException;
    User getUserInfo(int userId) throws UserException;
}
