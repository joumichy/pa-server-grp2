package com.server.grp2.Security;

import com.server.grp2.User.domain.UserRepository;
import com.server.grp2.User.domain.model.User;
import com.server.grp2.User.domain.model.UserException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    UserRepository userRepository;

    public UserDetailsServiceImpl(UserRepository userRepository){
        this.userRepository = userRepository;
    }

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String username) {
        User user = null;
        try {
            user = userRepository.getUserInfo(username);
        } catch (UserException e) {
            e.printStackTrace();
        }

        assert user != null;
        return UserDetailsImpl.build(user);
    }}
