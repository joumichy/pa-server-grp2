package com.server.grp2.Authentication.infrastructure.controller;


import com.server.grp2.Authentication.uses_case.AuthenticateUser;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/auth")
public class AuthenticationController {

    private final AuthenticateUser authenticateUser;

    public AuthenticationController(AuthenticateUser authenticateUser) {
        this.authenticateUser = authenticateUser;
    }

    @PostMapping("/signin")
    public ResponseEntity<JwtResponse> authenticateUser(@Valid @RequestBody LoginDto loginDto) {

        JwtResponse errorResponse = new JwtResponse(
                "401",
                "Unauthorized",
                "Error: Unauthorized");

        JwtResponse userAuthenticated = authenticateUser.execute(loginDto.getUsername(), loginDto.getPassword());

        if ((userAuthenticated.getAccessToken() == null) |
                userAuthenticated.getId() == null |
                userAuthenticated.getEmail().equals("") |
                userAuthenticated.getUsername().equals("")) {
            return new ResponseEntity<>(
                    errorResponse,
                    HttpStatus.UNAUTHORIZED
            );
        } else {
            return new ResponseEntity<>(
                    userAuthenticated,
                    HttpStatus.OK);
        }
    }

}
