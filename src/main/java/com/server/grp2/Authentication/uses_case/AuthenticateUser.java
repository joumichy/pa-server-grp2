package com.server.grp2.Authentication.uses_case;

import com.server.grp2.Authentication.infrastructure.controller.JwtResponse;
import com.server.grp2.Security.UserDetailsImpl;
import com.server.grp2.Security.JwtFactory;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.stream.Collectors;

@Service
public class AuthenticateUser {

    private final AuthenticationManager authenticationManager;

    private final JwtFactory jwtFactory;

    public AuthenticateUser(AuthenticationManager authenticationManager, JwtFactory jwtFactory) {
        this.authenticationManager = authenticationManager;
        this.jwtFactory = jwtFactory;
    }

    public JwtResponse execute(String username, String password){
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(username, password));

        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtFactory.generateJwtToken(authentication);

        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        String roles = userDetails.getAuthorities()
                .stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.joining());
        return new JwtResponse(jwt,
                userDetails.getId(),
                userDetails.getUsername(),
                userDetails.getEmail(),
                roles);
    }

}
