package com.server.grp2.Search.infrastructure.dao;

import javax.persistence.*;

@Entity
@Table(name = "statesearch")
public class StateSearchEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "statesearch_id")
    private long id;

    @Column(name = "user_id")
    private long userId;

    @Column(name = "search_id")
    private long searchResultId;

    private String state;

    public StateSearchEntity(long userId, long searchResultId, String state){
        this.userId = userId;
        this.searchResultId = searchResultId;
        this.state = state;
    }

    public StateSearchEntity() {
    }

    public long getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public long getSearchResultId() {
        return searchResultId;
    }

    public void updateState(String state) {
        this.state = state;
    }

    public String getState() {
        return state;
    }
}
