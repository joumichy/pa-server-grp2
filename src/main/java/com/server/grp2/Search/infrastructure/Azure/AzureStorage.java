package com.server.grp2.Search.infrastructure.Azure;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.microsoft.azure.storage.CloudStorageAccount;
import com.microsoft.azure.storage.StorageException;
import com.microsoft.azure.storage.queue.CloudQueue;
import com.microsoft.azure.storage.queue.CloudQueueClient;
import com.microsoft.azure.storage.queue.CloudQueueMessage;
import com.server.grp2.Search.domain.model.Search;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.net.URISyntaxException;
import java.security.InvalidKeyException;

@Component
public class AzureStorage {

    private static String queueName;

    private static String storageConnectionString;

    public AzureStorage( @Value("${SEND_TASKS_QUEUE}") String queueName,  @Value("${STORAGE_CONNECTION_STRING}") String storageConnectionString){
        AzureStorage.queueName = queueName;
        AzureStorage.storageConnectionString = storageConnectionString;
    }

    public static void createQueueAndSendMessageToQueue(Search searchDto) throws StorageException, JsonProcessingException, URISyntaxException, InvalidKeyException {
        String jsonString;
        ObjectMapper Obj = new ObjectMapper();

        CloudStorageAccount storageAccount = CloudStorageAccount.parse(storageConnectionString);
        CloudQueueClient queueClient = storageAccount.createCloudQueueClient();
        CloudQueue queue = queueClient.getQueueReference(queueName + searchDto.getUsername().toLowerCase());
        queue.createIfNotExists();
        jsonString = Obj.writeValueAsString(searchDto);
        CloudQueueMessage message = new CloudQueueMessage(jsonString);
        queue.addMessage(message);
    }

}
