package com.server.grp2.Search.infrastructure.controller;

import java.io.Serializable;

public class SearchDto implements Serializable {

    private String urlVideo;

    private String categoryVideo;

    private String youtuberName;

    private String keyWords;

    private String username;

    private int idSearch;

    private int idUser;

    public SearchDto(String urlVideo, String categoryVideo, String youtuberName, String keyWords, int idsearch, int iduser) {
        this.urlVideo = urlVideo;
        this.categoryVideo = categoryVideo;
        this.youtuberName = youtuberName;
        this.keyWords = keyWords;
        this.idSearch = idsearch;
        this.idUser = iduser;
    }

    public SearchDto() {
        this.urlVideo = "";
        this.categoryVideo = "";
        this.youtuberName = "";
        this.keyWords = "";
        this.username = "";
        this.idSearch = 0;
        this.idUser = 0;
    }

    @Override
    public String toString() {
        return "{" +
                "urlVideo:" +"this.urlVideo+"+
                ", categoryVideo:" + this.categoryVideo + '\'' +
                ", youtuberName:" + this.youtuberName + '\'' +
                ", keyWords:"+ this.keyWords + '\'' +
                ", username:'" + this.username + '\'' +
                ", idSearch:'" + this.idSearch + '\'' +
                ", idUser:'" + this.idUser + '\'' +
                '}';
    }

    public String getUrlVideo() {
        return urlVideo;
    }

    public String getCategoryVideo() {
        return categoryVideo;
    }

    public String getYoutuberName() {
        return youtuberName;
    }

    public String getKeyWords() {
        return keyWords;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getIdUser() {
        return idUser;
    }

}
