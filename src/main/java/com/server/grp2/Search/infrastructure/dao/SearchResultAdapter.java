package com.server.grp2.Search.infrastructure.dao;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.server.grp2.Search.domain.model.Search;
import com.server.grp2.Search.domain.model.SearchFind;
import com.server.grp2.Search.domain.model.SearchResult;
import com.server.grp2.User.domain.model.User;
import com.server.grp2.User.infrastructure.dao.UserEntity;

import java.util.LinkedList;
import java.util.Queue;

public class SearchResultAdapter {

    public static Queue<SearchResultEntity> convertToSearchResultQueue(JsonNode searchResultJsonNode, UserEntity user) throws JsonProcessingException {
        Queue<SearchResultEntity> queueSearchResultToReturn = new LinkedList<>();
        ObjectMapper objectMapper = new ObjectMapper();

        String urlfindFieldToJson = searchResultJsonNode.get("urlfind").toPrettyString();

        Queue<SearchFind> videoFindQueue = objectMapper.readValue(urlfindFieldToJson, new TypeReference<Queue<SearchFind>>(){});

        long idSearchFromUrlSearch = searchResultJsonNode.get("urlsearch").get("idSearch").asInt();

        SearchFind searchResult;
        while ( (searchResult = videoFindQueue.poll()) != null){
            queueSearchResultToReturn.add(new SearchResultEntity(
                    idSearchFromUrlSearch,
                    user.getUserId(),
                    searchResultJsonNode.get("urlsearch").get("urlVideo").asText(),
                    searchResultJsonNode.get("urlsearch").get("categoryVideo").asText(),
                    searchResultJsonNode.get("urlsearch").get("youtuberName").asText(),
                    searchResultJsonNode.get("urlsearch").get("keyWords").asText(),
                    searchResult.getUrlVideo(),
                    searchResult.getCategoryVideo(),
                    searchResult.getYoutuberName(),
                    searchResult.getKeyWords(),
                    searchResult.getUrlVignette()
            ));
            idSearchFromUrlSearch++;
        }

        return queueSearchResultToReturn;
    }


    public static SearchResultEntity convertToSearchResultEntity(Search search, UserEntity user){
        return new SearchResultEntity(
                0,
                user.getUserId(),
                search.getUrlVideo(),
                search.getCategoryVideo(),
                search.getYoutuberName(),
                search.getKeyWords(),
                null,
                null,
                null,
                null,
                null);
    }

    public static SearchResult convertToSearchResult(SearchResultEntity search, User user) {

        return new SearchResult(
                search.getIdSearch(),
                Long.parseLong(user.getId()),
                search.getUrlvideoSearch(),
                search.getCategorySearch(),
                search.getYoutuberNameSearch(),
                search.getKeyWordsSearch(),
                search.getUrlvideoFind(),
                search.getCategoryFind(),
                search.getYoutuberNameFind(),
                search.getKeyWordsFind(),
                search.getUrlVignette()
        );
    }
}
