package com.server.grp2.Search.infrastructure.dao;

import com.server.grp2.Search.domain.model.SearchResult;
import com.server.grp2.Search.domain.model.StateSearch;
import com.server.grp2.User.infrastructure.dao.UserEntity;

public class StateSearchAdapter {

    public static StateSearchEntity convertToStateSearchEntity(StateSearch stateSearch, UserEntity userEntity, SearchResult searchResult){
        return new StateSearchEntity(
                userEntity.getUserId(),
                searchResult.getId(),
                stateSearch.getState());
    }

    public static StateSearch convertToSearchState(StateSearchEntity stateSearchEntity){
        return new StateSearch(
                Long.toString(stateSearchEntity.getId()),
                stateSearchEntity.getSearchResultId(),
                stateSearchEntity.getUserId(),
                stateSearchEntity.getState()
        );
    }

}
