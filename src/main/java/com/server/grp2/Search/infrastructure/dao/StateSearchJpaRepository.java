package com.server.grp2.Search.infrastructure.dao;

import com.server.grp2.Search.domain.StateSearchRepository;
import com.server.grp2.Search.domain.model.SearchResult;
import com.server.grp2.Search.domain.model.StateSearch;
import com.server.grp2.User.domain.model.RoleException;
import com.server.grp2.User.domain.model.User;
import com.server.grp2.User.infrastructure.dao.RoleAdapter;
import com.server.grp2.User.infrastructure.dao.RoleEntity;
import com.server.grp2.User.infrastructure.dao.UserAdapter;
import com.server.grp2.User.infrastructure.dao.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public interface StateSearchJpaRepository extends JpaRepository<StateSearchEntity, Long>, StateSearchRepository {

    default StateSearch createState(StateSearch stateSearch, User user, SearchResult searchResult) throws RoleException {

        RoleEntity role = RoleAdapter.convertToRoleEntity(user.getRole().getRoleName());
        UserEntity userEntity = UserAdapter.convertToUserEntity(user, role);
        userEntity.updateId(Long.parseLong(user.getId()));

        List<StateSearchEntity> stateSearchEntityList = findAll();

        for (StateSearchEntity stateSearchEntity : stateSearchEntityList){
            if(stateSearchEntity.getSearchResultId() == stateSearch.getIdSearch()){
                stateSearchEntity.updateState(stateSearch.getState());
                save(stateSearchEntity);
                return stateSearch;
            }
        }
        StateSearchEntity stateSearchEntity = StateSearchAdapter.convertToStateSearchEntity(stateSearch,userEntity, searchResult);
        save(stateSearchEntity);
        return stateSearch;
    }

    default List<StateSearch> getTenLastStateSearch(int userId) {
        List<StateSearch> lastTenSearchState = new ArrayList<>();
        int compteur = 0;
        List<StateSearchEntity> allStateSearchEntity = findAll();

        for (StateSearchEntity stateSearchEntity : allStateSearchEntity){
            if(stateSearchEntity.getUserId() == userId  && compteur < 11){
                lastTenSearchState.add(StateSearchAdapter.convertToSearchState(stateSearchEntity));
                compteur++;
            }
        }
        return lastTenSearchState;
    }

}
