package com.server.grp2.Search.infrastructure.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.microsoft.azure.storage.StorageException;
import com.server.grp2.Search.domain.model.*;
import com.server.grp2.Search.use_cases.*;
import com.server.grp2.User.domain.model.RoleException;
import com.server.grp2.User.domain.model.UserException;
import com.server.grp2.User.infrastructure.dao.UserEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URISyntaxException;
import java.security.InvalidKeyException;
import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/search")
public class SearchController {

    GetResults getResults;

    GetStateSearch getStateSearch;

    CreateSearch createSearch;

    AddResults addResults;

    UpdateStateSearch updateStateSearch;

    public SearchController(GetResults getResults,
                            GetStateSearch getStateSearch,
                            CreateSearch createSearch,
                            AddResults addResults,
                            UpdateStateSearch updateStateSearch)
    {
        this.getResults = getResults;
        this.getStateSearch = getStateSearch;
        this.createSearch = createSearch;
        this.addResults = addResults;
        this.updateStateSearch = updateStateSearch;
    }


    @PostMapping("/newtask")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> addNewTask(@Valid @RequestBody SearchDto searchDto) throws URISyntaxException, StorageException, InvalidKeyException, JsonProcessingException {
        try {

            createSearch.execute(
                    searchDto.getUrlVideo(),
                    searchDto.getCategoryVideo(),
                    searchDto.getYoutuberName(),
                    searchDto.getKeyWords(),
                    searchDto.getIdUser()
            );
        }catch (TaskException | UserException | RoleException e){
            return new ResponseEntity<>(
                    e.getMessage(),
                    HttpStatus.FOUND);
        }
        return new ResponseEntity<>(
                "New search added ! ",
                HttpStatus.CREATED);
    }

    @PostMapping("/poststate")
    @PreAuthorize("hasRole('ADMIN')")
    public boolean postStateOfSearchResult(@RequestBody StateSearchDto stateSearchDto) throws RoleException, UserException, TaskException {
        updateStateSearch.execute(stateSearchDto.getIdSearch(),stateSearchDto.getIdUser(), stateSearchDto.getState());
        return true;
    }

    @GetMapping("/getresult")
    @PreAuthorize("hasRole('USER')")
    public List<SearchResult> getResult(@RequestHeader("url_search") String url_search ,
                                        @RequestHeader("youtubername_search") String youtubername_search,
                                        @RequestHeader("category_search") String category_search,
                                        @RequestHeader("keywords_search") String keywords_search,
                                        @RequestHeader("userId") UserEntity user) throws UserException {

        return getResults.execute(url_search, youtubername_search,category_search,keywords_search, user);

    }


    @PostMapping("/postnewtask")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<?> registerResults(@RequestBody String searchResultJsonStringFormat) throws JsonProcessingException, UserException, RoleException {
        String bodyToreturn = addResults.execute(searchResultJsonStringFormat);
        return new ResponseEntity<>(
                bodyToreturn,
                HttpStatus.OK);
    }

    @GetMapping("/getsearch/{iduser}")
    @PreAuthorize("hasRole('USER')")
    public List<StateSearchAndSearch> getRecentStateSearch(@PathVariable int iduser) throws UserException, SearchResultException {

        return getStateSearch.execute(iduser);
    }

}