package com.server.grp2.Search.infrastructure.dao;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.microsoft.azure.storage.StorageException;
import com.server.grp2.Search.domain.SearchResultRepository;
import com.server.grp2.Search.domain.model.Search;
import com.server.grp2.Search.domain.model.SearchResult;
import com.server.grp2.Search.domain.model.TaskException;
import com.server.grp2.Search.infrastructure.Azure.AzureStorage;
import com.server.grp2.User.domain.model.RoleException;
import com.server.grp2.User.domain.model.User;
import com.server.grp2.User.infrastructure.dao.RoleAdapter;
import com.server.grp2.User.infrastructure.dao.RoleEntity;
import com.server.grp2.User.infrastructure.dao.UserAdapter;
import com.server.grp2.User.infrastructure.dao.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.net.URISyntaxException;
import java.security.InvalidKeyException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Queue;

@Repository
public interface SearchResultJpaRepository extends JpaRepository<SearchResultEntity, Long>, SearchResultRepository {

    default String createSearchResult(User user, JsonNode searchResultJsonNode) throws JsonProcessingException, RoleException {
        RoleEntity roleEntity = RoleAdapter.convertToRoleEntity(user.getRole().getRoleName());
        UserEntity userEntity = UserAdapter.convertToUserEntity(user, roleEntity);
        userEntity.updateId(Long.parseLong(user.getId()));

        Queue<SearchResultEntity> searchResultEntityQueue;
        searchResultEntityQueue = SearchResultAdapter.convertToSearchResultQueue(searchResultJsonNode, userEntity);
        Iterator<SearchResultEntity> waitingQueueIterator = searchResultEntityQueue.iterator();
        do{
            SearchResultEntity searchResultEntity;
            searchResultEntity = searchResultEntityQueue.poll();
            save(searchResultEntity);
        }while(waitingQueueIterator.hasNext());

        return searchResultJsonNode.toPrettyString();
    }

    default SearchResult addResearch(Search search, User user) throws URISyntaxException, StorageException, InvalidKeyException, JsonProcessingException, TaskException, RoleException {

        RoleEntity roleEntity = RoleAdapter.convertToRoleEntity(user.getRole().getRoleName());
        UserEntity userEntity = UserAdapter.convertToUserEntity(user, roleEntity);
        userEntity.updateId(Long.parseLong(user.getId()));

        List<SearchResultEntity> allSearchResultEntities = findAll();
        for (SearchResultEntity searchResultEntity : allSearchResultEntities){
            if (searchResultEntity.getCategorySearch().equals(search.getCategoryVideo())
                    && searchResultEntity.getUrlvideoSearch().equals(search.getUrlVideo())
                    && searchResultEntity.getYoutuberNameSearch().equals(search.getYoutuberName())
                    && searchResultEntity.getKeyWordsSearch().equals(search.getKeyWords())
                    && searchResultEntity.getUserId() == userEntity.getUserId()){

                throw new TaskException("Search already present in database");
            }
        }

        SearchResultEntity searchResultEntity = SearchResultAdapter.convertToSearchResultEntity(search, userEntity);
        SearchResultEntity searchResultEntitySaved = save(searchResultEntity);

        search.updateUsernameAndSearchIdAndUserId(user.getUsername(), Long.toString(searchResultEntitySaved.getIdSearch()), Integer.parseInt(user.getId()));
        AzureStorage.createQueueAndSendMessageToQueue(search);

        return SearchResultAdapter.convertToSearchResult(searchResultEntitySaved, user);
    }

    default List<SearchResult> getSearchResults(String urlSearch, String youtuberNameSearch, String categorySearch, String keywordsSearch, User user){

        List<SearchResultEntity> allSearchResult = findAll();
        List<SearchResult> searchResultToReturn = new ArrayList<>();

        for (SearchResultEntity searchResultEntity : allSearchResult){
            if (searchResultEntity.getCategorySearch().equals(categorySearch) &&
                searchResultEntity.getUrlvideoSearch().equals(urlSearch) &&
                searchResultEntity.getYoutuberNameSearch().equals(youtuberNameSearch) &&
                searchResultEntity.getKeyWordsSearch().equals(keywordsSearch) &&
                searchResultEntity.getUserId() == Long.parseLong(user.getId()))
            {
                searchResultToReturn.add(SearchResultAdapter.convertToSearchResult(searchResultEntity, user));
            }
        }
        return searchResultToReturn;
    }

    default SearchResult findSearchResult(int id, User user) throws TaskException {
        List<SearchResultEntity> searchResultEntityList = findAll();

        for (SearchResultEntity searchResultEntity:searchResultEntityList){
            if (searchResultEntity.getIdSearch() == id) return SearchResultAdapter.convertToSearchResult(searchResultEntity, user);
        }

        throw new TaskException("Update failed ! ");

    }

    default List<SearchResult> findSearchResult(User user, long idSearch) {
       List<SearchResultEntity> searchResultEntityList = findAll();
       List<SearchResult> searchResultList = new ArrayList<>();

        for (SearchResultEntity searchResultEntity : searchResultEntityList){
           if (searchResultEntity.getIdSearch()==idSearch && searchResultEntity.getUserId() == Long.parseLong(user.getId())){
               searchResultList.add(SearchResultAdapter.convertToSearchResult(searchResultEntity,user));
           }
       }

        return searchResultList;
    }


}
