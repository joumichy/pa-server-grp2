package com.server.grp2.Search.infrastructure.controller;

public class StateSearchDto {

    private int idSearch;

    private int idUser;

    private String state;

    public StateSearchDto(int idSearch, int idUser, String state) {
        this.idSearch = idSearch;
        this.idUser = idUser;
        this.state = state;
    }

    public StateSearchDto() {
        this.idSearch = 0;
        this.idUser = 0;
        this.state = "";
    }


    public int getIdSearch() {
        return idSearch;
    }

    public int getIdUser() {
        return idUser;
    }

    public String getState() {
        return state;
    }

}
