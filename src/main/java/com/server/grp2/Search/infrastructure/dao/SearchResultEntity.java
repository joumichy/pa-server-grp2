package com.server.grp2.Search.infrastructure.dao;

import javax.persistence.*;

@Entity
@Table(name = "searchresultat")
public class SearchResultEntity {

    @Id
    @Column(name="searchresultat_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long idSearch;

    @Column(name = "user_id")
    private long userId;

    @Column(name = "url_search")
    private String urlvideoSearch;

    @Column(name = "category_search")
    private String categorySearch;

    @Column(name = "youtubername_search")
    private String youtuberNameSearch;

    @Column(name = "keywords_search")
    private String keyWordsSearch;

    @Column(name = "url_find")
    private String urlvideoFind;

    @Column(name = "category_find")
    private String categoryFind;

    @Column(name = "youtubername_find")
    private String youtuberNameFind;

    @Column(name = "keywords_find")
    private String keyWordsFind;

    @Column(name = "url_vignette")
    private String urlVignette;

    public SearchResultEntity(long idSearch, long userId, String urlvideoSearch, String categorySearch, String youtuberNameSearch, String keyWordsSearch, String urlvideoFind, String categoryFind, String youtuberNameFind, String keyWordsFind, String urlVignette) {
        this.idSearch = idSearch;
        this.userId = userId;
        this.urlvideoSearch = urlvideoSearch;
        this.categorySearch = categorySearch;
        this.youtuberNameSearch = youtuberNameSearch;
        this.keyWordsSearch = keyWordsSearch;
        this.urlvideoFind = urlvideoFind;
        this.categoryFind = categoryFind;
        this.youtuberNameFind = youtuberNameFind;
        this.keyWordsFind = keyWordsFind;
        this.urlVignette = urlVignette;
    }

    public SearchResultEntity() {}

    public long getIdSearch() {
        return idSearch;
    }

    public void setIdSearch(Long id) {
        this.idSearch = id;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getUrlvideoSearch() {
        return urlvideoSearch;
    }

    public String getCategorySearch() {
        return categorySearch;
    }

    public String getYoutuberNameSearch() {
        return youtuberNameSearch;
    }

    public String getKeyWordsSearch() {
        return keyWordsSearch;
    }

    public String getUrlvideoFind() {
        return urlvideoFind;
    }

    public String getCategoryFind() {
        return categoryFind;
    }

    public String getYoutuberNameFind() {
        return youtuberNameFind;
    }

    public String getKeyWordsFind() {
        return keyWordsFind;
    }

    public String getUrlVignette() {
        return urlVignette;
    }
}
