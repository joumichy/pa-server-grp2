package com.server.grp2.Search.domain.model;

import java.util.List;

public class StateSearchAndSearch {

    StateSearch stateSearch;

    List<SearchResult> searchResult;

    public StateSearchAndSearch(StateSearch stateSearch, List<SearchResult> searchResult) {
        this.stateSearch = stateSearch;
        this.searchResult = searchResult;
    }

    public StateSearch getStateSearch() {
        return stateSearch;
    }

    public List<SearchResult> getSearchResult() {
        return searchResult;
    }
}
