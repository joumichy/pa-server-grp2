package com.server.grp2.Search.domain.model;

public class SearchResultException extends Exception {
    public SearchResultException(String message) {
        super(message);
    }
}
