package com.server.grp2.Search.domain.model;

public class Search {

    private String idSearch;

    private int idUser;

    private final String urlVideo;

    private final String categoryVideo;

    private final String youtuberName;

    private final String keyWords;

    private String username;

    public Search(String idSearch, int idUser, String urlVideo, String categoryVideo, String youtuberName, String keyWords, String username) {
        this.idSearch = idSearch;
        this.idUser = idUser;
        this.urlVideo = urlVideo;
        this.categoryVideo = categoryVideo;
        this.youtuberName = youtuberName;
        this.keyWords = keyWords;
        this.username = username;
    }

    public String getIdSearch() {
        return idSearch;
    }

    public String getUrlVideo() {
        return urlVideo;
    }

    public String getCategoryVideo() {
        return categoryVideo;
    }

    public String getYoutuberName() {
        return youtuberName;
    }

    public String getKeyWords() {
        return keyWords;
    }

    public String getUsername() {
        return username;
    }

    public int getIdUser() {
        return idUser;
    }

    public void updateUsernameAndSearchIdAndUserId(String username, String idSearch, int userId) {
        this.idSearch = idSearch;
        this.idUser = userId;
        this.username = username;

    }
}
