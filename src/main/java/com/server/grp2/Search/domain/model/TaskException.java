package com.server.grp2.Search.domain.model;

public class TaskException extends Exception {

    public TaskException(String message) {
        super(message);
    }

}
