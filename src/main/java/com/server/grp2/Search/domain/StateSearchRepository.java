package com.server.grp2.Search.domain;

import com.server.grp2.Search.domain.model.SearchResult;
import com.server.grp2.Search.domain.model.StateSearch;
import com.server.grp2.User.domain.model.RoleException;
import com.server.grp2.User.domain.model.User;

import java.util.List;

public interface StateSearchRepository {
    StateSearch createState(StateSearch stateSearch, User user, SearchResult searchResult) throws RoleException;

    List<StateSearch> getTenLastStateSearch(int userId);
}
