package com.server.grp2.Search.domain.model;

public class StateSearch {

    private final String id;

    private final long idSearch;

    private final long idUser;

    private final String state;

    public StateSearch(String id, long idSearch, long idUser, String state) {
        this.id = id;
        this.idSearch = idSearch;
        this.idUser = idUser;
        this.state = state;
    }

    public long getIdSearch() {
        return this.idSearch;
    }

    public String getState() {
        return state;
    }

    public long getIdUser() {
        return idUser;
    }

    public String getId() {
        return id;
    }
}
