package com.server.grp2.Search.domain;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.microsoft.azure.storage.StorageException;
import com.server.grp2.Search.domain.model.Search;
import com.server.grp2.Search.domain.model.SearchResult;
import com.server.grp2.Search.domain.model.SearchResultException;
import com.server.grp2.Search.domain.model.TaskException;
import com.server.grp2.User.domain.model.RoleException;
import com.server.grp2.User.domain.model.User;
import com.server.grp2.User.domain.model.UserException;

import java.net.URISyntaxException;
import java.security.InvalidKeyException;
import java.util.List;

public interface SearchResultRepository {

    String createSearchResult(User user, JsonNode searchResultStringJsonNode) throws JsonProcessingException, UserException, RoleException;

    SearchResult addResearch(Search searchResult, User user) throws URISyntaxException, StorageException, InvalidKeyException, JsonProcessingException, TaskException, RoleException;

    List<SearchResult> getSearchResults(String urlSearch, String youtuberNameSearch, String categorySearch, String keywordsSearch, User user);

    SearchResult findSearchResult(int id, User user) throws TaskException;

    List<SearchResult> findSearchResult(User user, long idSearch) throws SearchResultException;
}
