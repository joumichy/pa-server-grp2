package com.server.grp2.Search.domain.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SearchFind {

    private String urlVideo;

    private String categoryVideo;

    private String youtuberName;

    private String keyWords;

    private String urlVignette;


    public SearchFind(String urlVideo, String categoryVideo, String youtuberName, String keyWords, String urlVignette) {
        this.urlVideo = urlVideo;
        this.categoryVideo = categoryVideo;
        this.youtuberName = youtuberName;
        this.keyWords = keyWords;
        this.urlVignette = urlVignette;
    }

    public SearchFind(){}

    public String getUrlVideo() {
        return urlVideo;
    }

    public String getCategoryVideo() {
        return categoryVideo;
    }

    public String getYoutuberName() {
        return youtuberName;
    }

    public String getKeyWords() {
        return keyWords;
    }

    public String getUrlVignette() {
        return urlVignette;
    }
}
