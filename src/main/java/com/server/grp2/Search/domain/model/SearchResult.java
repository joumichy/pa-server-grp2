package com.server.grp2.Search.domain.model;

public class SearchResult {

    private final long id;

    private final long userId;

    private final String urlVideoSearch;

    private final String categorySearch;

    private final String youtuberNameSearch;

    private final String keyWordsSearch;

    private final String urlvideoFind;

    private final String categoryFind;

    private final String youtuberNameFind;

    private final String keyWordsFind;

    private final String urlVignette;

    public SearchResult(long id, long userId, String urlVideoSearch, String categorySearch, String youtuberNameSearch, String keyWordsSearch, String urlvideoFind,  String categoryFind, String youtuberNameFind, String keyWordsFind, String urlVignette) {
        this.id = id;
        this.userId = userId;
        this.urlVideoSearch = urlVideoSearch;
        this.categorySearch = categorySearch;
        this.youtuberNameSearch = youtuberNameSearch;
        this.keyWordsSearch = keyWordsSearch;
        this.urlvideoFind = urlvideoFind;
        this.categoryFind = categoryFind;
        this.youtuberNameFind = youtuberNameFind;
        this.keyWordsFind = keyWordsFind;
        this.urlVignette = urlVignette;
    }

    public String getUrlVideoSearch() {
        return urlVideoSearch;
    }

    public String getCategorySearch() {
        return categorySearch;
    }

    public String getYoutuberNameSearch() {
        return youtuberNameSearch;
    }

    public String getKeyWordsSearch() {
        return keyWordsSearch;
    }

    public String getUrlVignette() {
        return urlVignette;
    }

    public long getId() {
        return id;
    }

    public long getUserId() {
        return userId;
    }

    public String getUrlvideoFind() {
        return urlvideoFind;
    }

    public String getCategoryFind() {
        return categoryFind;
    }

    public String getYoutuberNameFind() {
        return youtuberNameFind;
    }

    public String getKeyWordsFind() {
        return keyWordsFind;
    }
}
