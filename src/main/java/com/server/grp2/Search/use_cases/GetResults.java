package com.server.grp2.Search.use_cases;

import com.server.grp2.Search.domain.SearchResultRepository;
import com.server.grp2.Search.domain.model.SearchResult;
import com.server.grp2.User.domain.model.User;
import com.server.grp2.User.domain.model.UserException;
import com.server.grp2.User.infrastructure.dao.UserAdapter;
import com.server.grp2.User.infrastructure.dao.UserEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GetResults {

    private SearchResultRepository searchResultRepository;

    public GetResults(SearchResultRepository searchResultRepository){
        this.searchResultRepository = searchResultRepository;
    }


    public List<SearchResult> execute(String urlSearch, String youtuberNameSearch, String categorySearch, String keywordsSearch, UserEntity userEntity) throws UserException {
        User user = UserAdapter.convertToUser(userEntity);
        return searchResultRepository.getSearchResults(urlSearch,youtuberNameSearch,categorySearch,keywordsSearch,user);
    }

}
