package com.server.grp2.Search.use_cases;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.microsoft.azure.storage.StorageException;
import com.server.grp2.Search.domain.model.Search;
import com.server.grp2.Search.domain.model.TaskException;
import com.server.grp2.Search.domain.SearchResultRepository;
import com.server.grp2.User.domain.model.RoleException;
import com.server.grp2.User.domain.model.User;
import com.server.grp2.User.domain.model.UserException;
import com.server.grp2.User.domain.UserRepository;
import org.springframework.stereotype.Service;

import java.net.URISyntaxException;
import java.security.InvalidKeyException;

@Service
public class CreateSearch {

    private final UserRepository userRepository;

    private final SearchResultRepository searchResultRepository;

    public CreateSearch(UserRepository userRepository, SearchResultRepository searchResultRepository) {
        this.userRepository = userRepository;
        this.searchResultRepository = searchResultRepository;
    }

    public void execute (String urlVideo, String categoryVideo, String youtuberName, String keywords, int idUser) throws URISyntaxException, StorageException, InvalidKeyException, JsonProcessingException, TaskException, UserException, RoleException {
       User user = userRepository.getUserInfo(idUser);
       Search search = new Search(null, idUser, urlVideo, categoryVideo, youtuberName,keywords, user.getUsername());
       searchResultRepository.addResearch(search, user);
    }
}
