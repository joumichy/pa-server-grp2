package com.server.grp2.Search.use_cases;

import com.server.grp2.Search.domain.SearchResultRepository;
import com.server.grp2.Search.domain.StateSearchRepository;
import com.server.grp2.Search.domain.model.SearchResult;
import com.server.grp2.Search.domain.model.StateSearch;
import com.server.grp2.Search.domain.model.TaskException;
import com.server.grp2.User.domain.UserRepository;
import com.server.grp2.User.domain.model.RoleException;
import com.server.grp2.User.domain.model.User;
import com.server.grp2.User.domain.model.UserException;
import org.springframework.stereotype.Service;

@Service
public class UpdateStateSearch {

    private final StateSearchRepository stateSearchRepository;
    private final UserRepository userRepository;
    private final SearchResultRepository searchResultRepository;


    public UpdateStateSearch(StateSearchRepository stateSearchRepository,UserRepository userRepository, SearchResultRepository searchResultRepository) {
        this.stateSearchRepository = stateSearchRepository;
        this.userRepository = userRepository;
        this.searchResultRepository = searchResultRepository;
    }

    public StateSearch execute(int idSearch, int idUser, String state) throws UserException, RoleException, TaskException {
        User user = userRepository.getUserInfo(idUser);
        SearchResult searchResultBySearchId = searchResultRepository.findSearchResult(idSearch, user);
        StateSearch stateSearch = new StateSearch(null,idSearch,idUser, state);
        return stateSearchRepository.createState(stateSearch, user, searchResultBySearchId);

    }
}
