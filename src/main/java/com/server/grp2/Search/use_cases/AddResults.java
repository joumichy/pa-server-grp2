package com.server.grp2.Search.use_cases;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.server.grp2.Search.domain.SearchResultRepository;
import com.server.grp2.User.domain.model.RoleException;
import com.server.grp2.User.domain.model.User;
import com.server.grp2.User.domain.model.UserException;
import com.server.grp2.User.domain.UserRepository;
import org.springframework.stereotype.Service;

@Service
public class AddResults {

    private final UserRepository userRepository;

    private final SearchResultRepository searchResultRepository;

    private final ObjectMapper objectMapper;

    public AddResults(UserRepository userRepository, SearchResultRepository searchResultRepository, ObjectMapper objectMapper) {
        this.userRepository = userRepository;
        this.searchResultRepository = searchResultRepository;
        this.objectMapper = objectMapper;
    }

    public String execute(String searchResultJsonStringFormat) throws JsonProcessingException, UserException, RoleException {

        JsonNode searchResultStringJsonNode = objectMapper.readTree(searchResultJsonStringFormat);

        String username = searchResultStringJsonNode.get("urlsearch").get("username").asText();

        User user = userRepository.getUserInfo(username);

        return searchResultRepository.createSearchResult(
                user,
                searchResultStringJsonNode);
    }
}
