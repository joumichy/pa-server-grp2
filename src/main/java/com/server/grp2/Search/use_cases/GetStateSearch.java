package com.server.grp2.Search.use_cases;

import com.server.grp2.Search.domain.SearchResultRepository;
import com.server.grp2.Search.domain.StateSearchRepository;
import com.server.grp2.Search.domain.model.SearchResult;
import com.server.grp2.Search.domain.model.SearchResultException;
import com.server.grp2.Search.domain.model.StateSearch;
import com.server.grp2.Search.domain.model.StateSearchAndSearch;
import com.server.grp2.User.domain.UserRepository;
import com.server.grp2.User.domain.model.User;
import com.server.grp2.User.domain.model.UserException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class GetStateSearch {

    private StateSearchRepository stateSearchRepository;
    private SearchResultRepository searchResultRepository;
    private UserRepository userRepository;


    public GetStateSearch(StateSearchRepository stateSearchRepository, SearchResultRepository searchResultRepository, UserRepository userRepository){
        this.stateSearchRepository = stateSearchRepository;
        this.searchResultRepository = searchResultRepository;
        this.userRepository = userRepository;
    }

    public List<StateSearchAndSearch> execute(int userid) throws UserException, SearchResultException {

        User user = userRepository.getUserInfo(userid);
        List<StateSearchAndSearch> stateSearchAndSearchList = new ArrayList<>();
        List<SearchResult> searchResult;

        List<StateSearch> stateSearchList = stateSearchRepository.getTenLastStateSearch(userid);
        for (StateSearch stateSearch : stateSearchList){
            long idSearch = stateSearch.getIdSearch();
            searchResult = searchResultRepository.findSearchResult(user, idSearch);
            stateSearchAndSearchList.add(new StateSearchAndSearch(stateSearch, searchResult));

        }


        return stateSearchAndSearchList;
    }



}
