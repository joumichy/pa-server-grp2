package com.server.grp2.unitTests.controller;

import com.server.grp2.User.infrastructure.dao.UserJpaRepository;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.MockitoAnnotations.initMocks;

@ContextConfiguration
@AutoConfigureMockMvc
@SpringBootTest
@EnableAutoConfiguration
public class SearchResultAdapterTest {

/*
    @Autowired
    UserJpaRepository userJpaRepository;
*/

//    @BeforeEach
  //  public void setUp() {
  //      initMocks(this);

  //  }

    /*@Test
    public void convertToSearchResultQueue_should_convert_JsonNode_to_SearchResultQueue() throws JsonProcessingException {
        //given
        String json =
                "{\"urlsearch\":" +
                        "{\"idSearch\":158," +
                        "\"urlVideo\":\"https://www.youtube.com/watch?v=M8oaSEn5xug\"," +
                        "\"categoryVideo\":\"Gaming\"," +
                        "\"youtuberName\":\"dahmien7\",\"keyWords\":\"eft, escapefromtarkov, dahmien7\"," +
                        "\"username\":\"usertest\"," +
                        "\"idUser\":1" +
                "}," +
                "\"urlfind\":" +
                        "[{" +
                        "\"urlVideo\":\"https://www.youtube.com/watch?v=M8oaSEn5xug\"," +
                        "\"categoryVideo\":\"\"," +
                        "\"youtuberName\":\"dahmien7\"," +
                        "\"keyWords\":\"escapefromtarkov, eft, dahmien7\"," +
                        "\"urlVignette\":\"https://i.ytimg.com/vi/M8oaSEn5xug/default.jpg\"" +
                        "}, " +
                        "{\"urlVideo\":\"https://www.youtube.com/watch?v=u3WK1jvFJYo\"," +
                        "\"categoryVideo\":\"Gaming\"," +
                        "\"youtuberName\":\"dahmien7\"," +
                        "\"keyWords\":\"escapefromtarkov, eft, dahmien7\"," +
                        "\"urlVignette\":\"https://i.ytimg.com/vi/u3WK1jvFJYo/default.jpg\"}"+"]}";
        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode searchResultStringJsonNode = objectMapper.readTree(json);
        String urlfind = searchResultStringJsonNode.get("urlfind").toPrettyString();
        String username = searchResultStringJsonNode.get("urlsearch").get("username").asText();
        UserEntity user = jpaUserRepository.getUserByUsername(username);
        Queue<SearchResult> queueExpected = objectMapper.readValue(urlfind, new TypeReference<Queue<SearchResult>>(){});
        SearchResult firstElementExpected = queueExpected.poll();
        SearchResult secondElementExpected = queueExpected.poll();


        //when
        Queue<SearchResultEntity> resultQueue = SearchResultAdapter.convertToSearchResultQueue(searchResultStringJsonNode,user);

        //then
        assertThat(resultQueue.poll()).isEqualTo(firstElementExpected);

    }*/
}
