package com.server.grp2.unitTests.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.server.grp2.User.infrastructure.dao.RoleEntity;
import com.server.grp2.Authentication.infrastructure.controller.JwtResponse;
import com.server.grp2.Security.JwtFactory;
import com.server.grp2.User.domain.model.RoleException;
import com.server.grp2.Security.UserDetailsImpl;
import com.server.grp2.Authentication.infrastructure.controller.LoginDto;
import com.server.grp2.User.infrastructure.controller.UserController;
import com.server.grp2.User.infrastructure.controller.UserDto;
import com.server.grp2.Authentication.uses_case.AuthenticateUser;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.is;
import static org.mockito.BDDMockito.given;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@ContextConfiguration
@AutoConfigureMockMvc
@SpringBootTest
@EnableAutoConfiguration
public class UserControllerTest {

/*
    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    AuthenticationManager authenticationManager;

    @Mock
    @Autowired
    AuthenticateUser userService;

    @Autowired
    JwtFactory jwtFactory;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    Set<String> role;

    private UserController userController;

    Set<RoleEntity> roleSet;

    UserDto userTest;

    JwtResponse jwtResponse;

    @BeforeEach
    public void setUp() throws RoleException {
        role = new HashSet<>();
        role.add("user");
     //   userTest = new UserDto("usernameTest", "passwordTest", "firstnameTest", "lastnameTest", "testmail@gmail.fr", role);

        //userController = new UserController(userService, createRole);

        initMocks(this);
        mockMvc = webAppContextSetup(webApplicationContext).build();
    }

    @AfterEach
    public void cleanUpDatabase() {
       // userController.deleteUser(userTest.getUsername());
    }

    @Test
    public void should_return_201_response_and_newUser_when_register_user() throws Exception {

       // UserEntity user = UserAdapter.convertToUserEntity(userTest, roleSet);

        //given(userService.registerUser(userTest, roleSet)).willReturn(user);

        mockMvc.perform(post("/api/auth/signup")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(userTest))
                .characterEncoding("utf-8"))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.mail", is(userTest.getMail())))
                .andExpect(jsonPath("$.username", is(userTest.getUsername())))
                .andExpect(jsonPath("$.firstName", is(userTest.getFirstName())))
                .andExpect(jsonPath("$.lastName", is(userTest.getLastName())));
        //.andExpect(jsonPath("$.role", is(new String[]{"\"roleName\":\"ROLE_USER\""})));
    }

    @Test
    public void should_return_501_response_when_username_already_use() {
        //given
        userController.registerUser(userTest);
        //when
        ResponseEntity<?> responseWithUsernameAlreadyUsed = userController.registerUser(userTest);
        //then
        assertThat(responseWithUsernameAlreadyUsed.getBody()).isEqualTo(userTest.getUsername() + " already use !");
        assertThat(responseWithUsernameAlreadyUsed.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);

    }

    @Test
    public void should_return_ok_response_when_user_authenticate() throws Exception {
        userController.registerUser(userTest);

        LoginDto loginDto = new LoginDto(userTest.getUsername(), userTest.getPassword());
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginDto.getUsername(), loginDto.getPassword()));

        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtFactory.generateJwtToken(authentication);

        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        String roles = userDetails.getAuthorities()
                .stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.joining());
        JwtResponse jwtResponse = new JwtResponse(
                jwt,
                userDetails.getId(),
                userDetails.getUsername(),
                userDetails.getEmail(),
                roles);

        given(userService.execute(loginDto.getUsername(), loginDto.getPassword())).willReturn(jwtResponse);

        mockMvc.perform(post("/api/auth/signin")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(loginDto))
                .characterEncoding("utf-8"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(jwtResponse.getId().intValue())))
                .andExpect(jsonPath("$.username", is(jwtResponse.getUsername())))
                .andExpect(jsonPath("$.email", is(jwtResponse.getEmail())))
                .andExpect(jsonPath("$.roles", is(jwtResponse.getRoles())))
                .andExpect(jsonPath("$.tokenType", is(jwtResponse.getTokenType())));
                //.andExpect(jsonPath("$.accessToken", is(jwtResponse.getAccessToken())));
    }

   */
/* @Test
    public void should_return_401_response_when_user_authenticate_with_bad_credentials() throws Exception {
        LoginDto loginDto = new LoginDto(userTest.getUsername(), "wrongPassword");
        jwtResponse = new JwtResponse(
                "401",
                "Unauthorized",
                "Error: Unauthorized"
        );

        userController.registerUser(userTest);

        given(userService.execute(loginDto)).willReturn(jwtResponse);

        mockMvc.perform(post("/api/auth/signin")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(loginDto))
                .characterEncoding("utf-8"))
                .andExpect(status().isUnauthorized());


    }*//*


    @Test
    public void should_return_error_message_when_any_field_is_empty_during_sign_up() {

        //given
       */
/* UserDto userDtoWithEmptyUsername = new UserDto("", "password", "Marc", "Lastname", "m@mail.fr", role);
        UserDto userDtoWithEmptyPassword = new UserDto("username", "", "John", "Lastname", "m@mail.fr", role);
        UserDto userDtoWithEmptyFirstname = new UserDto("username", "password", "", "Lastname", "m@mail.fr", role);
        UserDto userDtoWithEmptyLastname = new UserDto("username", "password", "Valentin", "", "m@mail.fr", role);
        UserDto userDtoWithEmptyMail = new UserDto("username", "password", "Valentin", "Lastname", "", role);*//*


        //when
       */
/* final ResponseEntity<?> responseNoUsername = userController.registerUser(userDtoWithEmptyUsername);
        final ResponseEntity<?> responseNoPassword = userController.registerUser(userDtoWithEmptyPassword);
        final ResponseEntity<?> responseNoFirstname = userController.registerUser(userDtoWithEmptyFirstname);
        final ResponseEntity<?> responseNoLastname = userController.registerUser(userDtoWithEmptyLastname);
        final ResponseEntity<?> responseNoMail = userController.registerUser(userDtoWithEmptyMail);*//*


        //then
   */
/*     assertThat(responseNoUsername.getBody()).isEqualTo("Field missing");
        assertThat(responseNoPassword.getBody()).isEqualTo("Field missing");
        assertThat(responseNoFirstname.getBody()).isEqualTo("Field missing");
        assertThat(responseNoLastname.getBody()).isEqualTo("Field missing");
        assertThat(responseNoMail.getBody()).isEqualTo("Field missing");*//*


    }

    @Test
    public void should_return_error_message_when_mail_is_already_used() {

        userController.registerUser(userTest);

    //    UserDto userDtoWithMailAlreadyUsed = new UserDto("username", "password", "Marc", "Lastname", "testmail@gmail.fr", role);

      //  ResponseEntity<?> responseMailAlreadyUsed = userController.registerUser(userDtoWithMailAlreadyUsed);

        //assertThat(responseMailAlreadyUsed.getBody()).isEqualTo(userTest.getMail() + " already use !");
    }

    @Test
    public void should_return_200_Response_when_user_is_delete() {
        userController.registerUser(userTest);

      //  ResponseEntity<?> responseDelete = userController.deleteUser(userTest.getUsername());

    //    assertThat(responseDelete.getBody()).isEqualTo("User deleted ! ");
      //  assertThat(responseDelete.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Test
    public void should_return_500_Response_when_username_doesnt_exist() {
        userController.registerUser(userTest);

     //   ResponseEntity<?> responseDelete = userController.deleteUser("");

      //  assertThat(responseDelete.getBody()).isEqualTo("username not find or user doesn't exist");
    //    assertThat(responseDelete.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    }

*/
}
