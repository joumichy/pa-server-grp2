package com.server.grp2.unitTests.controller;

import com.server.grp2.User.infrastructure.dao.UserEntity;
import com.server.grp2.User.infrastructure.controller.UserDto;
import com.server.grp2.User.infrastructure.dao.RoleEntity;
import com.server.grp2.User.domain.model.RoleException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ContextConfiguration;

import java.util.HashSet;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.MockitoAnnotations.initMocks;

@ContextConfiguration
@AutoConfigureMockMvc
@SpringBootTest
@EnableAutoConfiguration
public class UserAdapterTest {

/*
    Set<RoleEntity> roleSet;

    Set<String> role;

    @Autowired
    PasswordEncoder encoder;

    @BeforeEach
    public void setUp() throws RoleException {
        role = new HashSet<>();
        role.add("user");
        initMocks(this);

    }

*/
    @Test
    public void convertToUser_should_convert_UserDto_into_User() {
        //given
        //UserDto userDtoToConvert = new UserDto("usernameTest", "passwordTest", "firstnameTest", "lastnameTest", "testmail@gmail.fr", role);
       // UserEntity userExpected = new UserEntity(userDtoToConvert.getUsername(),encoder.encode("passwordTest"), userDtoToConvert.getFirstName(), userDtoToConvert.getLastName(), userDtoToConvert.getMail(),true, roleSet);

        //when
        //UserEntity user = UserAdapter.convertToUserEntity(userDtoToConvert, roleSet);

        //then
        /*assertThat(user.getUsername()).isEqualTo(userExpected.getUsername());
        //assertThat(user.getPassword()).isEqualTo(userExpected.getPassword());
        assertThat(user.getFirstName()).isEqualTo(userExpected.getFirstName());
        assertThat(user.getLastName()).isEqualTo(userExpected.getLastName());
        assertThat(user.getMail()).isEqualTo(userExpected.getMail());
        assertThat(user.getRoleEntity()).isEqualTo(userExpected.getRoleEntity());*/



    }


}
