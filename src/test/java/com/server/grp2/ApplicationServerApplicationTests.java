package com.server.grp2;

import org.junit.jupiter.api.Test;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(classes = ApplicationServerApplication.class)
@EnableAutoConfiguration
public class ApplicationServerApplicationTests {

    @Test
    void contextLoads() {
    }
}
